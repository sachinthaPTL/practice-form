import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class PersonalInformationPageTest {
    WebDriver driver;

    @BeforeTest
    public void beforeTest(){
        System.setProperty("webdriver.chrome.driver","drivers/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("C:\\Users\\PTL_PC\\Desktop\\Form\\Pragmatic Test Labs(03).html");
        driver.manage().window().maximize();
    }

    @Test
    public void personalInformation(){
        driver.findElement(By.id("firstname")).sendKeys("Sachintha");
        driver.findElement(By.id("lastname")).sendKeys("Lakmal");
        driver.findElement(By.id("email")).sendKeys("test@gmail.com");
        driver.findElement(By.id("areacode")).sendKeys("011");
        driver.findElement(By.id("phonenumber")).sendKeys("1234567");
        driver.findElement(By.id("male")).click();
        driver.findElement(By.id("oneyear")).click();
        driver.findElement(By.id("fileupload")).sendKeys("C:\\Users\\PTL_PC\\Desktop\\Form\\PTL-Logo-2.png");
        driver.findElement(By.id("seleniumIDE")).click();
        driver.findElement(By.id("seleniumwebdriver")).click();
        driver.findElement(By.id("seleniumgrid")).click();
        driver.findElement(By.id("continents")).sendKeys("Asia");
        driver.findElement(By.id("btnsubmit")).click();
    }

}
